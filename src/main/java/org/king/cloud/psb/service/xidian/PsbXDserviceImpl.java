package org.king.cloud.psb.service.xidian;

import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.king.cloud.psb.dao.WeixinWxpsbDao;
import org.king.cloud.psb.pojo.Wxpms;
import org.king.cloud.psb.service.IWeixinService;
import org.king.cloud.psb.service.impl.SmsServiceImpl;
import org.king.cloud.psb.service.xidian.util.AESUtil;
import org.king.cloud.psb.service.xidian.util.HttpRequestUtil;
import org.king.cloud.psb.utils.JsonListMapUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


@Service("psbXDservice")
public class PsbXDserviceImpl {
	
	protected Logger logger = Logger.getLogger(this.getClass());
	
	@Resource(name = "smsService")
	SmsServiceImpl smsService;
	@Resource(name="iweixinService")
    IWeixinService iweixinService;
	/*@Value("${com.xidian.key}")
	private String xdKey;*/
	
	@Resource
	WeixinWxpsbDao weixinWxpmsDao;
	 public JSONObject selectWxpmsByPrimaryKey(String id){
	    	Wxpms s;
	    	s=  iweixinService.selectWxpmsByPrimaryKey(id);
	    	String [] str=s.getStr1().split(",");
	    	String str2 = s.getStr2().replace("{", "").replace("}", "");
	    	String strx = str[0]+","+str[3]+","+","+str2+","+str[1]+",appkey:\""+s.getAppkey()+"\",username:\""+s.getUsername()+"\",passwords:\""+s.getPasswords()+"\","+str[2]+","+str[4]+","+str[5]+","+str[6]+","+str[7]+","+str[8]+","+str[9];
	    	JSONObject obj = JSON.parseObject(strx);
	    	
	    	 return obj;
	     }
	
	
	
	
	
	/**
	 * 入住
	 * @param obj
	 * @return
	 */
	public String checkIn(JSONObject obj) {
	
		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		obj.put("hotelCode", selectWxpmsByPrimaryKey.get("hotelCode"));
		String xdKey = selectWxpmsByPrimaryKey.get("key").toString();
		//System.out.println(xdKey);
		
		HashMap<Object, Object> resultData = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		//String str = "{\"address\":\"XXXXXXXXXXXXXXXX\",\"birthdate\":\"1994-07-11\",\"certificateCode\":\"410410410410410410\",\"checkinTime\":\"2018-05-24\",\"checkoutTime\":\"\",\"hotelCode\":\"CS-001\",\"nation\":\"汉族\",\"nativeCode\":\"410381\",\"phone\":\"15110047705\",\"realName\":\"史毅博\",\"roomCode\":\"0511\",\"serialNum\":\"123\",\"sex\":\"男\"}";

		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		String jsonString = jsonObject.toJSONString();
		String encrypt = "";
		String result = "";
		String realName = jsonObject.getString("realName");
		String certificateCode = jsonObject.getString("certificateCode");
		String hotelId = jsonObject.getString("hotelId");
		try {

			encrypt = AESUtil.encrypt(jsonString, xdKey);//"cs123456"
//			encrypt = AESUtil.encrypt(jsonString, "cs123456");//
		    map.put("data", encrypt);

			String url = "http://47.98.160.70:9101/openApi/v1/hotel/checkin";
			result = HttpRequestUtil.send(url, map, "UTF-8");
			JSONObject json = JSONObject.parseObject(result);
			if("100".equals(json.getString("code"))){
				System.out.println("XD-----------success");
				resultData.put("state",1);
				resultData.put("msg", "success");
				resultData.put("data",json.get("data"));
			}else{
				//对于调用失败的处理
				//result = HttpRequestUtil.send(url, map, "UTF-8");
				resultData.put("state",0);
				resultData.put("msg", "error");
				String content = "【西点】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
				smsService.send("18612239449", content);
				logger.info(new Date() + " 【西点】入住上传psb失败，入住酒店号是:"+hotelId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state",0);
			resultData.put("msg", "error");
			String content = "【西点】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
			smsService.send("18612239449", content);
			logger.info(new Date() + " 【西点】入住上传psb失败，入住酒店号是:"+hotelId);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
	}
	
	
	/**
	 * 退房
	 * @param obj
	 * @return
	 */
	public String checkout (JSONObject obj) {

		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		obj.put("hotelCode", selectWxpmsByPrimaryKey.get("hotelCode"));
		String xdKey = selectWxpmsByPrimaryKey.get("key").toString();
		//System.out.println(xdKey);
		
		HashMap<Object, Object> resultData = new HashMap<>();
		HashMap<String, String> map = new HashMap<>();
		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		String jsonString = jsonObject.toJSONString();
		String encrypt = "";
		String result = "";
		String serialNum = jsonObject.getString("serialNum");
		String idCard = jsonObject.getString("idCard");
		String hotelId = jsonObject.getString("hotelId");
		try {
			encrypt = AESUtil.encrypt(jsonString,  xdKey);
			map.put("data", encrypt);
			String url = "http://47.98.160.70:9101/openApi/v1/hotel/checkout";
			result = HttpRequestUtil.send(url, map, "UTF-8");
			JSONObject json = JSONObject.parseObject(result);
			if("100".equals(json.getString("code"))){
				System.out.println("退房成功");
				resultData.put("state", 1);
				resultData.put("msg", "success");
			}else{
				resultData.put("state", 0);
				resultData.put("msg", "error");
				String content = "【西点】退房上传psb失败，入住酒店号是"+hotelId+"，"+"流水号是"+ serialNum+"，"+"入住人身份证是"+idCard+"!";
				smsService.send("18612239449", content);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state", 0);
			resultData.put("msg", "error");
			String content = "【西点】退房上传psb失败，入住酒店号是"+hotelId+"，"+"流水号是"+ serialNum+"，"+"入住人身份证是"+idCard+"!";
			smsService.send("18612239449", content);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
	}
	
	
	
}
