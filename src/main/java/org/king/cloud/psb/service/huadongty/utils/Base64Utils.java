package org.king.cloud.psb.service.huadongty.utils;

import com.migcomponents.migbase64.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Base64实用工具
 *
 * <p>基于com.migcomponents.migbase64</p>
 *
 * @author LiXiaoyong on 2017年8月24日 下午2:05:28
 * @version Version 1.1
 */
public class Base64Utils {
    
    /**
     * 默认字符集名称
     */
    private static final String DEFAULT_CHARSET = "UTF-8";
    

    /**
     * Base64编码
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待编码文本
     * @return {@code String} 类型编码结果
     */
    public static String encode(final String src) {
        return encode(src, DEFAULT_CHARSET);
    }
    
    /**
     * Base64编码
     * @param src 待编码文本
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return {@code String} 类型编码结果
     */
    public static String encode(final String src, final String charset) {
        if (src == null || src.length() == 0) {
            return "";
        }
        
        try {
            return Base64.encodeToString(src.getBytes(charset), false);
        } catch (UnsupportedEncodingException excpt) {
            excpt.printStackTrace();
            return "";
        }
    }

    /**
     * Base64编码
     * @param src 待编码数据
     * @return {@code String} 类型编码结果，{@code src} 为 {@code null} 时返回 {@code ""}
     */
    public static String encode(final byte[] src) {
        return src == null ? "" : Base64.encodeToString(src, false);
    }
    
    /**
     * Base64编码
     * @param src 待编码数据
     * @return {@code byte[]} 类型编码结果，{@code src} 为 {@code null} 时返回 {@code null}
     */
    public static byte[] encodeBytes(final byte[] src) {
        return src == null ? null : Base64.encodeToByte(src, false);
    }
    
    
    /**
     * Base64解码
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待解码文本
     * @return {@code String} 类型解码结果，{@code src} 为空时返回  {@code ""}
     */
    public static String decode(final String src) {
        return decode(src, DEFAULT_CHARSET);
    }
    
    /**
     * Base64解码
     * @param src 待解码文本
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return {@code String} 类型解码结果，{@code src} 为空时返回  {@code ""}
     */
    public static String decode(final String src, final String charset) {
        if (src == null || src.length() == 0) {
            return "";
        }
        
        try {
            return new String(Base64.decode(src), charset);
        } catch (UnsupportedEncodingException excpt) {
            excpt.printStackTrace();
            return "";
        }
    }

    /**
     * Base64解码
     * @param src 待解码文本
     * @return {@code byte[]} 类型解码结果，{@code src} 为空时返回  {@code null}
     */
    public static byte[] decodeBytes(final String src) {
        if (src == null || src.length() == 0) {
            return null;
        }
        return Base64.decode(src);
    }

    /**
     * Base64解码
     * @param src 待解码数据
     * @return {@code byte[]} 类型解码结果，{@code src} 为 {@code null} 时返回  {@code null}
     */
    public static byte[] decodeBytes(final byte[] src) {
        return src == null ? null : Base64.decode(src);
    }
    
    
    /**
     * 对Base64字符串进行URL编码
     * @param src Base64字符串
     * @return {@code '/'} 替换为 {@code '_'}，{@code '+'} 替换为 {@code '-'}
     */
    @Deprecated
    public static String urlEncode(final String src) {
        if (src == null || src.trim().length() <= 0) {
            return "";
        }
        return src.replace('+', '-').replace('/', '_');
    }
    
    /**
     * 对Base64字符串进行URL解码
     * @param src URL编码后的Base64字符串
     * @return {@code '_'} 替换为 {@code '/'}，{@code '-'} 替换为 {@code '+'}
     */
    @Deprecated
    public static String urlDecode(final String src) {
        if (src == null || src.trim().length() <= 0) {
            return "";
        }
        return src.replace('-', '+').replace('_', '/');
    }
    
    
    /**
     * URL编码器
     *
     * @author LiXiaoyong on 2018年1月18日 下午3:05:50
     * @version Version 1.0
     */
    public static class UrlEncoder {
        
        /**
         * 对Base64字符串进行URL编码
         * @param src Base64字符串
         * @return {@code '/'} 替换为 {@code '_'}，{@code '+'} 替换为 {@code '-'}
         */
        public static String encode(final String src) {
            if (src == null || src.trim().length() <= 0) {
                return "";
            }
            return src.replace('+', '-').replace('/', '_');
        }
        
        /**
         * 对Base64字符串进行URL解码
         * @param src URL编码后的Base64字符串
         * @return {@code '_'} 替换为 {@code '/'}，{@code '-'} 替换为 {@code '+'}
         */
        public static String decode(final String src) {
            if (src == null || src.trim().length() <= 0) {
                return "";
            }
            return src.replace('-', '+').replace('_', '/');
        }
        
    }
    
}
