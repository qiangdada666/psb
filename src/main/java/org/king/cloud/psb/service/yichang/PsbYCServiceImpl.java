package org.king.cloud.psb.service.yichang;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.axiom.om.OMElement;
import org.king.cloud.psb.service.yichang.httpCilent.DocumentClient;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

@Service("psbYCService")
public class PsbYCServiceImpl {

	/**
	 * 入住
	 * @param obj
	 * @return
	 */
	public String checkIn(JSONObject obj) {
		
		HashMap<Object, Object> resultData = new HashMap<>();
		
		 String[] paramStrArray = new String[] {"name", "sex", "nation", "birthday", "expired", "pid", "djsj", "hotelno", "roomno", "phone", "address", "photo", "sphoto", "cflag"};
	        // 参数值
	        String[] valStrArray = new String[] {obj.getString("realName"),"男".equals(obj.getString("sex"))?"1":"2",obj.getString("nation"),obj.getString("birthdate"),"2010.01.02-2020.01.02",obj.getString("certificateCode"),new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),obj.getString("hotelno"),"603",obj.getString("phone"),obj.getString("address"),obj.getString("photo"),obj.getString("sphoto"),"N"};

        OMElement result = DocumentClient.getInstance().invokeRampartService("gnlkAdd",paramStrArray,valStrArray);
		if(result!=null && "登记成功".equals(result.getFirstElement().getText())){
			System.out.println("YC-----------success");
			resultData.put("state", 1);
			resultData.put("msg", "success");
			
		}else{
			resultData.put("state",0);
			resultData.put("msg", "error");
		}
        return resultData.toString();
	}
	
	public String checkout(JSONObject obj) {
		HashMap<Object, Object> resultData = new HashMap<>();
		 String[] paramStrArray = new String[] {"pid", "djsj", "tfsj", "hotelno"};
	        // 参数值
	        String[] valStrArray = new String[] {obj.getString("idCard"),obj.getString("checkInTime"),new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),obj.getString("hotelno")};
	        OMElement result = DocumentClient.getInstance().invokeRampartService("checkOut",paramStrArray,valStrArray);
	        if(result!=null && "退房成功".equals(result.getFirstElement().getText())){
				System.out.println("YC-----------success");
				resultData.put("state", 1);
				resultData.put("msg", "success");
			}else{
				resultData.put("state",0);
				resultData.put("msg", "error");
			}
	        return resultData.toString();
	}

}
