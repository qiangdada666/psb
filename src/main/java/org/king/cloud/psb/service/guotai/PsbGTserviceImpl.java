package org.king.cloud.psb.service.guotai;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.ParseException;
import org.apache.log4j.Logger;
import org.king.cloud.psb.service.guotai.util.GThttpClient;
import org.king.cloud.psb.utils.JsonListMapUtil;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

@Service("psbGTservice")
public class PsbGTserviceImpl {
	private static Logger logger = Logger.getLogger(PsbGTserviceImpl.class);
	
	
	
	
	/**
	 * 入住  
	 * @param obj
	 * @return
	 */
	public String checkIn(JSONObject obj) {
		// TODO Auto-generated method stub
		
		
		
		String bir = "";
		String object = obj.getString("birthdate");
		String year = object.substring(0, 4);
		String yue = object.substring(4,6);
		String ri = object.substring(6);
		bir = year + "-" + yue + "-"+ri;
		HashMap<Object, Object> resultData = new HashMap<>();
		String url = "http://58.210.180.134:8891/HotelSer/CheckIn";
		JSONObject josn = new JSONObject();
		josn.put("pType", "0");
		josn.put("psbId", "1234567890");//obj.get("hotelno")
		josn.put("idNo", obj.get("certificateCode"));
		josn.put("name", obj.get("realName"));
		josn.put("sex", obj.get("sex"));
		josn.put("nationCode",obj.get("nation"));
		josn.put("idType","11");
		josn.put("province", obj.get("nativeCode"));
		josn.put("address", obj.get("address"));
		josn.put("checkInDate",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		josn.put("roomNo", obj.get("roomCode"));
		System.out.println( "房间号:--------"+obj.get("roomCode"));
		josn.put("photo","xxx");
		josn.put("ScenePhoto","xxxxxx");
		josn.put("Semblance", "0.99");
		josn.put("faceResult", "通过");
		josn.put("Operator","张三");
		josn.put("birth",bir);
		Map map = new HashMap();
		map.put("json",josn.toString());
		String result = "";
		try {
			result = GThttpClient.send(url,map,"UTF-8");
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if("1".equals(result)){
			System.out.println("GT-----------success");
			resultData.put("state", 1);
			resultData.put("msg", "success");
		}else{
			
			resultData.put("state",0);
			resultData.put("msg", "error");
			
		}
		String jsonString = JsonListMapUtil.toJSONString(resultData);
		return jsonString;
		
	}
	
	/**
	 * 退房
	 * @param obj
	 * @return
	 */
	public static String checkout(JSONObject obj) {
		HashMap<Object, Object> resultData = new HashMap<>();
		StringBuffer sb = new StringBuffer("");
		sb.append("psbId="+1234567890);
		sb.append("&roomNo="+obj.getString("roomCode"));//obj.getString("roomCode")
		sb.append("&cardNum="+obj.getString("idCard"));//+obj.getString("idCard")410381200007119678|370103198901028911
		sb.append("&checkOutDate="+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		//String paramStr = "psbId=1234567890"+"&roomNo=606"+"&cardNum=410381200007119678"+"&checkOutDate="+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String url = "http://58.210.180.134:8891/HotelSer/CheckOut";
		String result = "";
		try {
			result = GThttpClient.sendStr(url,sb.toString(),"UTF-8");
			System.out.println(result);
		} catch (ParseException | IOException e) {
			logger.error("调用国泰psb退房接口异常d！");
			e.printStackTrace();
			return "fail";
		}
		if("1".equals(result)){
			resultData.put("state", 1);
			resultData.put("msg", "success");
		}else{
			
			resultData.put("state",0);
			resultData.put("msg", "error");
			
		}
		String jsonString = JsonListMapUtil.toJSONString(resultData);
		return jsonString;
	}

	public static void main(String[] args) {
		checkout(new JSONObject());
	}
	
	
	
}
