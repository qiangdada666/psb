package org.king.cloud.psb.service.hefei;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.king.cloud.psb.dao.CodeTableHfDao;
import org.king.cloud.psb.dao.WeixinWxpsbDao;
import org.king.cloud.psb.pojo.Wxpms;
import org.king.cloud.psb.service.CodeTableHfService;
import org.king.cloud.psb.service.IWeixinService;
import org.king.cloud.psb.service.hefei.pojo.NbBean;
import org.king.cloud.psb.service.hefei.pojo.TfBean;
import org.king.cloud.psb.service.impl.SmsServiceImpl;
import org.king.cloud.psb.utils.JsonListMapUtil;
import org.king.cloud.psb.utils.RedisPool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service("psbHFService")
public class PsbHFServiceImpl {
	
	@Value("${hf.url}")
	private String url;
	protected Logger logger = Logger.getLogger(this.getClass());
	static String usercode =  "";//"3401030097";
	static String pwd = "";//"1234567";
	//static String url = "http://27.115.101.138:8088/lgyqt/services/lgyService";
	
	@Resource(name = "smsService")
	SmsServiceImpl smsService;
	@Resource(name="iweixinService")
    IWeixinService iweixinService;
	@Resource(name="codeTableHfService")
	CodeTableHfService codeTableHfService ;
	
	@Resource
	WeixinWxpsbDao weixinWxpmsDao;
	public JSONObject selectWxpmsByPrimaryKey(String id){
    	Wxpms s;
    	s=  iweixinService.selectWxpmsByPrimaryKey(id);
    	JSONObject obj2 = JSON.parseObject(s.getStr2());
    	return obj2;
	 }
	
	@Resource
	CodeTableHfDao codeTableHfDao;
	public String  selectByParamOfNation(String name,String type){
		String code = codeTableHfService.selectByParamOfNation(name, type);
		return code;
	}
	
	/**
	 * 入住
	 * @param obj
	 * @return
	 * @throws Exception 
	 */
	
	public String checkIn(JSONObject obj) throws Exception {
		
		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		usercode = selectWxpmsByPrimaryKey.get("usercode").toString();
		pwd = selectWxpmsByPrimaryKey.get("pwd").toString();
		
		MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] buff = m.digest(pwd.getBytes());
		String md5 = new String(Hex.encode(buff));

		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		JSONObject idCard = jsonObject.getJSONObject("obj");
		String result = "";
		String postid = "";
		String realName = idCard.getString("name");
		String certificateCode = idCard.getString("cardNum");
		String hotelId = jsonObject.getString("hotelId");
		HashMap<Object, Object> resultData = new HashMap<>();
		
		try {
		
			org.apache.axis.client.Call call1 = new org.apache.axis.client.Call(url);
			call1.setOperationName("postNWBRecrods");
			
			List<NbBean> list = new ArrayList<NbBean>();
			NbBean nb = new NbBean();
			SimpleDateFormat spm=new SimpleDateFormat("yyyyMMddhhmm");
			SimpleDateFormat spm1=new SimpleDateFormat("yyyy/MM/dd hh:mm");
			nb.setDjr("自助机");//登记人
			postid = "haoyizhu"+System.currentTimeMillis();
			nb.setPostid(postid);//设置唯一标识
			nb.setCsrq(idCard.getString("birthday"));
			//从数据字典中查询相应民族对应的编号
			nb.setMz(selectByParamOfNation(idCard.getString("nation")+"族", "mz"));//民族
			
			//System.out.println(selectByParamOfNation("汉"+"族", "mz"));		
			//System.out.println(codeTableHfDao.selectByParamOfNation2("汉"));
			
			nb.setPicture(jsonObject.getString("cardimg")); //旅客照片
			nb.setRzfh(jsonObject.getString("roomNum"));
			nb.setRzsj(spm.format(spm1.parse(jsonObject.getString("checkinDate"))));
			nb.setSsxq(idCard.getString("cardNum").substring(0, 6));//所属县区
			//nb.setTfsj(spm.format(spm1.parse(jsonObject.getString("checkoutDate"))));
			nb.setXb("男".equals(idCard.getString("sex"))?"1":"2");
			nb.setXm(idCard.getString("name"));
			nb.setXz(idCard.getString("address"));
			nb.setZjhm(idCard.getString("cardNum"));
			nb.setZjlx("11");//默认：身份证
			//以下参数可以为空
			//nb.setRzlc(1);//入住楼层
			nb.setLxdh("");//联系电话
			nb.setLkbm("");//住宿流水号
			nb.setFzqx("");//发证区县
			nb.setZy("");
			nb.setBz("");
			nb.setCphm("");
			nb.setCpsz("");
			nb.setHcl("");
			nb.setHcq("");
			nb.setLcsy("");
			nb.setSfsjdx("");
			nb.setXykhm("");
			nb.setXyklx("");
			
			list.add(nb);
			
			GsonBuilder build = new GsonBuilder();
			Gson gson = build.create();
			String json_list = gson.toJson(list);
			
			result = (String)call1.invoke(new Object[]{usercode,md5,"1", json_list});
		
			JSONObject json = JSONObject.parseObject(result);
			if("true".equals(json.getString("success"))){
				Object items = json.get("list");
				JSONArray itemList = (JSONArray) items;
				JSONObject listObject = itemList.getJSONObject(0);
				
				if("true".equals(listObject.getString("success"))){
					System.out.println("HEFEI-----------success");
					resultData.put("state",1);
					resultData.put("msg", "success");
					String liushui=RedisPool.get(jsonObject.getString("hotelId")+jsonObject.getString("orderId")+"psb");
					if(liushui==null){
						RedisPool.set(jsonObject.getString("hotelId")+jsonObject.getString("orderId")+"psb", listObject.get("lkbm").toString()+","+idCard.getString("cardNum")+","+postid);
					}else {
						RedisPool.set(jsonObject.getString("hotelId")+jsonObject.getString("orderId")+"psb", liushui+"-"+listObject.get("lkbm").toString()+","+idCard.getString("cardNum")+","+postid);
					}
					System.out.println(RedisPool.get(jsonObject.getString("hotelId")+jsonObject.getString("orderId")+"psb"));
					
					resultData.put("data",listObject.get("lkbm"));
				}else{
					resultData.put("state",0);
					resultData.put("msg", json.get("message"));
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", json.get("message"));
				String content = "【合肥】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
				smsService.send("18612239449", content);
				logger.info(new Date() + " 【华动泰越】入住上传psb失败，入住酒店号是:"+hotelId);
			}
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "error");
			String content = "【合肥】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
			smsService.send("18612239449", content);
			logger.info(new Date() + " 【合肥】入住上传psb失败，入住酒店号是:"+hotelId);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
	}
	
	
	/**
	 * 退房
	 * @param obj
	 * @return
	 */
	public String checkout (JSONObject obj) throws Exception{
		
		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		usercode = selectWxpmsByPrimaryKey.get("usercode").toString();
		pwd = selectWxpmsByPrimaryKey.get("pwd").toString();
		
		MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] buff = m.digest(pwd.getBytes());
		String md5 = new String(Hex.encode(buff));

		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		String result = "";
		//String realName = jsonObject.getString("realName");
		String liushui = "";
		String certificateCode =  ""; //jsonObject.getString("certificateCode");
		String hotelId = jsonObject.getString("hotelId");
		HashMap<Object, Object> resultData = new HashMap<>();
		
		try {
			String serialNum=  RedisPool.get(jsonObject.getString("hotelId")+jsonObject.getString("orderId")+"psb");
			String [] str=serialNum.split("-");
			for (int i = 0; i < str.length; i++) {
				String [] strte = str[i].split(",");
			
				org.apache.axis.client.Call call2 = new org.apache.axis.client.Call(url);
				call2.setOperationName("postLd");
				
				List<TfBean> list = new ArrayList<TfBean>();
				TfBean tf = new TfBean();
				SimpleDateFormat spm=new SimpleDateFormat("yyyyMMddhhmm");
				SimpleDateFormat spm1=new SimpleDateFormat("yyyy/MM/dd hh:mm");
				//tf.setLdsj(jsonObject.getString("checkoutTime").replace("-","").replace(":", "").replace(" ", "").substring(0, 12));
				//tf.setLkbm(jsonObject.getString("serialNum"));
				//tf.setPostid("haoyizhu");
				tf.setLdsj(spm.format(spm1.parse(jsonObject.getString("checkoutDate"))));
				tf.setLkbm(strte[0]);
				tf.setPostid(strte[2]);
				list.add(tf);
				liushui=strte[0];
				certificateCode = strte[1]; //身份证号
				GsonBuilder build = new GsonBuilder();
				Gson gson = build.create();
				String json_list = gson.toJson(list);
				
				result = (String)call2.invoke(new Object[]{usercode,md5,"1", json_list});
			
				JSONObject json = JSONObject.parseObject(result);
				if("true".equals(json.getString("success"))){
					Object items = json.get("list");
					JSONArray itemList = (JSONArray) items;
					JSONObject listObject = itemList.getJSONObject(0);
					if("true".equals(listObject.getString("success"))){
						System.out.println("HEFEI-----------success");
						resultData.put("state",1);
						resultData.put("msg", "success");
					}else{
						resultData.put("state",0);
						resultData.put("msg", json.get("message"));
					}
			    }else{
					resultData.put("state",0);
					resultData.put("msg", json.get("message"));
					String content = "【合肥】退房上传psb失败，入住酒店号是:"+hotelId+"，"+"流水号是:"+ liushui+"，"+"入住人身份证是:"+certificateCode+"!";
					smsService.send("18612239449", content);
					logger.info(new Date() + " 【合肥】退房上传psb失败，入住酒店号是:"+hotelId);
				}
		    }
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "error");
			String content = "【合肥】退房上传psb失败，入住酒店号是:"+hotelId+"，"+"流水号是:"+ liushui+"，"+"入住人身份证是:"+certificateCode+"!";
			smsService.send("18612239449", content);
			logger.info(new Date() + " 【合肥】退房上传psb失败，入住酒店号是:"+hotelId);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
	}
	
}
