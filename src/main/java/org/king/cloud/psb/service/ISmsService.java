package org.king.cloud.psb.service;

public interface ISmsService {
	public String send(String phone, String content);
}
