package org.king.cloud.psb.service.xidian.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class q {
	public static String sendHttpPost(String url, String body) throws Exception {
		body = "{data:'77VOeUxe3AGD/Vn7bNcmJdvd9wi72IJHMZ3/lnXLO8+eMonAXZlHIt5spbmttuIciBdn0TuZRcOodKYzydsoQvfPiWQpS8RxLZmgjwm7QPgMLSAQphDXt+2C5HkqXjCu9VwsADXM/AWLnJ81coyg8G4wVzvSsHydlwEbBXuhtSAdxc8kEUCeheTzxwDMtesBF2zONLn5iJsGmJ1bl4DDhBUNoUWLAyiRgm7l5krcCIR2wI+7itoA6KNyBZD8MVFDCeB7OyhasVILQzj2UsjbfvpD3kuyDF+i9w9BnGJwXnZmyC14gv6MgbztTW3GbKqduCwnWZ/RkNdgezIo1T+EDPz6E3PPvVNRQ0njvFvEp/twZYAyRxHfuqRokbiBV/zm8SywAG+/mR5hdZTbk6n77g=='}";
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("code", "CS-001");
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.setEntity(new StringEntity(body));

		CloseableHttpResponse response = httpClient.execute(httpPost);
		System.out.println(response.getStatusLine().getStatusCode() + "\n");
		HttpEntity entity = response.getEntity();
		String responseContent = EntityUtils.toString(entity, "UTF-8"); 
		System.out.println(responseContent);

		response.close();
		httpClient.close();
		return responseContent;
		}
}
