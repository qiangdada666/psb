package org.king.cloud.psb.service.hefei.pojo;

public class TfBean {
	
	private String postid;//唯一ID
	private String lkbm;  //流水号 
	private String ldsj;  //退房时间
	
	public String getPostid() {
		return postid;
	}
	public void setPostid(String postid) {
		this.postid = postid;
	}
	public String getLkbm() {
		return lkbm;
	}
	public void setLkbm(String lkbm) {
		this.lkbm = lkbm;
	}
	public String getLdsj() {
		return ldsj;
	}
	public void setLdsj(String ldsj) {
		this.ldsj = ldsj;
	}
}
