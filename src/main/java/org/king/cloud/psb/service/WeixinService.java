package org.king.cloud.psb.service;




import javax.annotation.Resource;

import org.king.cloud.psb.dao.WeixinWxpsbDao;
import org.king.cloud.psb.pojo.Wxpms;
import org.springframework.stereotype.Service;

@Service("iweixinService")
public class WeixinService implements IWeixinService {
	
	@Resource
	WeixinWxpsbDao weixinWxpsbDao;
	
	@Override
	public String selectByPrimaryKey(String id) {
		
		return weixinWxpsbDao.selectByPrimaryKey(id);
	}
	
	@Override
	public Wxpms selectWxpmsByPrimaryKey(String id) {
		
		return weixinWxpsbDao.selectWxpmsByPrimaryKey(id);
	}
	
	
}
