package org.king.cloud.psb.service;

import org.king.cloud.psb.pojo.Wxpms;


public interface IWeixinService {
	//通过id获取整条数据
	String selectByPrimaryKey(String id);

	Wxpms selectWxpmsByPrimaryKey(String id);
	
}
