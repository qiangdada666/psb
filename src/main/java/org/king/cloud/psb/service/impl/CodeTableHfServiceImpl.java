package org.king.cloud.psb.service.impl;




import javax.annotation.Resource;

import org.king.cloud.psb.dao.CodeTableHfDao;
import org.king.cloud.psb.service.CodeTableHfService;
import org.springframework.stereotype.Service;

@Service("codeTableHfService")
public class CodeTableHfServiceImpl implements CodeTableHfService{

	@Resource
	CodeTableHfDao codeTableHfDao;
	
	@Override
	public String selectByParamOfNation(String name,String types) {
		
		String selectByParamOfNation = codeTableHfDao.selectByParamOfNation(name);
		return selectByParamOfNation;
	}
	
	//@Override
	//public String selectByParamOfNation2(String names,String types) {
		
	//	String selectByParamOfNation = codeTableHfDao.selectByParamOfNation2(names);
	//	return selectByParamOfNation;
	//}
	
	
	
}
