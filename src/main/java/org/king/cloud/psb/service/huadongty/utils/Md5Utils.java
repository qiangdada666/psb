package org.king.cloud.psb.service.huadongty.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.hdtytech.jutils.TypeUtils;

/**
 * MD5实用工具
 *
 * @author LiXiaoyong on 2017年8月24日 下午5:16:32
 * @version Version 1.1
 */
public class Md5Utils {
    
    /**
     * 默认字符集名称
     */
    private static final String DEFAULT_CHARSET = "UTF-8";


    /**
     * MD5加密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待加密内容
     * @return MD5加密结果（32位小写）
     */
    public static String encrypt32(final String src) {
        return encrypt(src, DEFAULT_CHARSET);
    }
    
    /**
     * MD5加密
     * @param src 待加密内容
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return MD5加密结果（32位小写）
     */
    public static String encrypt32(final String src, final String charset) {
        return encrypt(src, charset);
    }
    

    /**
     * MD5加密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待加密内容
     * @return MD5加密结果（16位小写）
     */
    public static String encrypt16(final String src) {
        final String md5Str = encrypt(src, DEFAULT_CHARSET);
        if (md5Str.length() != 32) {
            return "";
        }
        return encrypt(src, DEFAULT_CHARSET).substring(8, 24);
    }
    
    /**
     * MD5加密
     * @param src 待加密内容
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return MD5加密结果（16位小写）
     */
    public static String encrypt16(final String src, final String charset) {
        final String md5Str = encrypt(src, charset);
        if (32 != md5Str.length()) {
            return "";
        }
        return encrypt(src, charset).substring(8, 24);
    }


    /**
     * MD5加密
     * @param src 待加密内容
     * @param charset 字符集名称
     * @return MD5加密结果（32位小写）
     */
    private static String encrypt(final String src, final String charset) {
        if (src == null || src.length() == 0) {
            return "";
        }

        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] input = src.getBytes(charset);
            messageDigest.update(input);
            final byte[] output = messageDigest.digest();
            return TypeUtils.bytes2HexStr(output);
        } catch (NoSuchAlgorithmException excpt) {
            excpt.printStackTrace();
            return "";
        } catch (UnsupportedEncodingException excpt) {
            excpt.printStackTrace();
            return "";
        }
    }

}
