package org.king.cloud.psb.service.impl;

import org.king.cloud.psb.service.ISmsService;
import org.king.cloud.psb.utils.SmsUtil;
import org.springframework.stereotype.Service;


@Service("smsService")
public class SmsServiceImpl implements ISmsService {

	@Override
	public String send(String phone, String content) {
		// TODO Auto-generated method stub
		String code = SmsUtil.sendSms(phone, content);
		return code;
	}

}
