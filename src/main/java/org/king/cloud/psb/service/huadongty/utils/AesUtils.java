package org.king.cloud.psb.service.huadongty.utils;

import com.hdtytech.jutils.StrUtils;
import com.hdtytech.jutils.TypeUtils;
import com.hdtytech.jutils.exception.AesException;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES实用工具
 *
 * <p>加密模式：CBC<br>
 * 填充规则：ZeroPadding<br>
 * 默认向量：Md5Utils.encrypt16(key)，key为密钥</p>
 *
 * @author LiXiaoyong on 2017年8月24日 下午5:47:30
 * @version Version 1.1
 */
public class AesUtils {

    /**
     * AES加密错误
     */
    private static final String AES_ENCRYPT_ERROR = "AES加密错误";
    /**
     * AES解密错误
     */
    private static final String AES_DECRYPT_ERROR = "AES解密错误";
    
    /**
     * 默认字符集名称
     */
    private static final String DEFAULT_CHARSET = "UTF-8";
    /**
     * 密钥长度
     */
    private static final int AES_KEY_SIZE = 16; // 128bit
    /**
     * 加密规则，使用NoPadding是为了兼容C#/JS/PHP的ZeroPadding
     */
    private static final String CIPHER_RULE = "AES/CBC/NoPadding"; // 方法/模式/填充规则


    /**
     * AES加密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待加密内容
     * @param password 密码（任意长度）
     * @return AES加密结果（Base64格式）
     * @throws AesException {@link AesException}
     */
    public static String encryptAnyKey(final String src, final String password) throws AesException {
        final String key = Md5Utils.encrypt16(password);
        return encryptEx(src, key.toLowerCase(Locale.ENGLISH), key.toUpperCase(Locale.ENGLISH));
    }

    /**
     * AES解密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待解密内容（Base64格式）
     * @param password 密码（任意长度）
     * @return AES解密结果
     * @throws AesException {@link AesException}
     */
    public static String decryptAnyKey(final String src, final String password) throws AesException {
        final String key = Md5Utils.encrypt16(password);
        return decryptEx(src, key.toLowerCase(Locale.ENGLISH), key.toUpperCase(Locale.ENGLISH));
    }

    
    /**
     * AES加密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待加密内容
     * @param key 密钥（16位）
     * @return AES加密结果（Base64格式）
     * @throws AesException {@link AesException}
     */
    public static String encrypt(final String src, final String key) throws AesException {
        return encryptEx(src, key, null, DEFAULT_CHARSET);
    }

    /**
     * AES加密
     * @param src 待加密内容
     * @param key 密钥（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES加密结果（Base64格式）
     * @throws AesException {@link AesException}
     */
    public static String encrypt(final String src, final String key, final String charset) throws AesException {
        return encryptEx(src, key, null, DEFAULT_CHARSET);
    }
    
    /**
     * AES加密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待加密内容
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @return AES加密结果（Base64格式）
     * @throws AesException {@link AesException}
     */
    public static String encryptEx(final String src, final String key, final String keyIv) throws AesException {
        return encryptEx(src, key, keyIv, DEFAULT_CHARSET);
    }
    
    /**
     * AES加密
     * @param src 待加密内容
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES加密结果（Base64格式）
     * @throws AesException {@link AesException}
     */
    public static String encryptEx(final String src, final String key, final String keyIv, final String charset) throws AesException {
        if (src == null || src.length() == 0) {
            return "";
        }

        byte[] input;
        try {
            input = src.getBytes(charset);
        } catch (UnsupportedEncodingException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        }

        final byte[] result = encryptToBytes(input, key, keyIv, charset);
        if (result != null) {
            return Base64Utils.encode(result);
        }
        return "";
    }

    
    /**
     * AES解密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待解密内容（Base64格式）
     * @param key 密钥（16位）
     * @return AES解密结果
     * @throws AesException {@link AesException}
     */
    public static String decrypt(final String src, final String key) throws AesException {
        return decryptEx(src, key, null, DEFAULT_CHARSET);
    }
    
    /**
     * AES解密
     * @param src 待解密内容（Base64格式）
     * @param key 密钥（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES解密结果
     * @throws AesException {@link AesException}
     */
    public static String decrypt(final String src, final String key, final String charset) throws AesException {
        return decryptEx(src, key, null, charset);
    }

    /**
     * AES解密
     * <p>使用默认字符集：UTF-8</p>
     * @param src 待解密内容（Base64格式）
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @return AES解密结果
     * @throws AesException {@link AesException}
     */
    public static String decryptEx(final String src, final String key, final String keyIv) throws AesException {
        return decryptEx(src, key, keyIv, DEFAULT_CHARSET);
    }
    
    /**
     * AES解密
     * @param src 待解密内容（Base64格式）
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES解密结果
     * @throws AesException {@link AesException}
     */
    public static String decryptEx(final String src, final String key, final String keyIv, final String charset) throws AesException {
        if (src == null || src.length() == 0) {
            return "";
        }

        final byte[] input = Base64Utils.decodeBytes(src);
        if (input == null) {
            throw new AesException("解密内容格式错误");
        }

        final byte[] result = decryptToBytes(input, key, keyIv, charset);
        return TypeUtils.bytes2Str(result, charset);
    }


    /**
     * AES加密
     * @param src 待加密数据
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES加密结果
     * @throws AesException {@link AesException}
     */
    private static byte[] encryptToBytes(final byte[] src, final String key, final String keyIv, final String charset) throws AesException {
        validateAesInput(src, key, keyIv);
        try {
            // 创建密钥
            final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(charset), "AES");

            // 创建密钥向量
            String ivValue = keyIv;
            if (ivValue == null) {
                ivValue = Md5Utils.encrypt16(key); // 默认密钥向量
            }
            final IvParameterSpec ivParameterSpec = new IvParameterSpec(ivValue.getBytes());

            // 创建并初始化密码器
            final Cipher cipher = Cipher.getInstance(CIPHER_RULE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            // 模拟ZeroPadding
            final byte[] input = zeroPadding(src, cipher.getBlockSize());

            // 加密
            return cipher.doFinal(input);
        } catch (UnsupportedEncodingException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (NoSuchPaddingException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (NoSuchAlgorithmException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (InvalidAlgorithmParameterException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (InvalidKeyException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (BadPaddingException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        } catch (IllegalBlockSizeException excpt) {
            throw new AesException(AES_ENCRYPT_ERROR, excpt);
        }
    }

    /**
     * AES解密
     * @param src 待解密数据
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @param charset 字符集名称，如：UTF-8、GB2312等
     * @return AES加密结果
     * @throws AesException {@link AesException}
     */
    private static byte[] decryptToBytes(final byte[] src, final String key, final String keyIv, final String charset) throws AesException {
        validateAesInput(src, key, keyIv);
        try {
            // 创建密钥
            final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(charset), "AES");

            // 创建密钥向量
            String ivValue = keyIv;
            if (StrUtils.isEmpty(ivValue)) {
                ivValue = Md5Utils.encrypt16(key); // 默认密钥向量
            }
            final IvParameterSpec ivParameterSpec = new IvParameterSpec(ivValue.getBytes(charset));

            // 创建并初始化密码器
            final Cipher cipher = Cipher.getInstance(CIPHER_RULE);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            // 解密
            return cipher.doFinal(src);
        } catch (UnsupportedEncodingException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (NoSuchPaddingException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (NoSuchAlgorithmException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (InvalidAlgorithmParameterException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (InvalidKeyException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (BadPaddingException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        } catch (IllegalBlockSizeException excpt) {
            throw new AesException(AES_DECRYPT_ERROR, excpt);
        }
    }

    
    /**
     * AES加密/解密输入验证
     * @param src 待加密/解密内容
     * @param key 密钥（16位）
     * @param keyIv 密钥向量（16位）
     * @throws AesException {@link AesException}
     */
    private static void validateAesInput(final byte[] src, final String key, final String keyIv) throws AesException {
        if (src == null) {
            throw new AesException("待加密/解密内容为空");
        }
        if (key == null) {
            throw new AesException("密钥为空");
        }
        if (key.length() != AES_KEY_SIZE) {
            throw new AesException("密钥长度错误");
        }
        if (keyIv != null && keyIv.trim().length() != AES_KEY_SIZE) {
            throw new AesException("密钥向量长度错误");
        }
    }
    
    /**
     * ZeroPadding
     * <p>为兼容JS、C#、PHP等，模拟ZeroPadding效果</p>
     * @param data 待加密数据
     * @param blockSize 加密数据块大小
     * @return ZeroPadding后的待加密数据
     */
    private static byte[] zeroPadding(final byte[] data, final int blockSize) {
        if (data == null) {
            return null;
        }

        int dataLen = data.length;
        final int missingNum = dataLen % blockSize;
        if (missingNum != 0) {
            dataLen += (blockSize - missingNum);
        }

        final byte[] result = new byte[dataLen];
        System.arraycopy(data, 0, result, 0, data.length);
        return result;
    }

}
