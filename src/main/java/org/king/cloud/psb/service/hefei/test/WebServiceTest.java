package org.king.cloud.psb.service.hefei.test;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.client.Call;
import org.king.cloud.psb.service.hefei.pojo.NbBean;
import org.king.cloud.psb.service.hefei.pojo.TfBean;
import org.springframework.security.crypto.codec.Hex;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WebServiceTest {
	
	static String usercode = "3401030097";
	static String pwd = "1234567";
	static String url = "http://27.115.101.138:8088/lgyqt/services/lgyService";
	
	public static void main(String[] args)  {
		
		try {
			test_getDict();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//查询字典
	public static void test_getDict() throws Exception{
		
			MessageDigest m = MessageDigest.getInstance("MD5");
			byte[] buff = m.digest(pwd.getBytes());
			String md5 = new String(Hex.encode(buff));

			Call call2 = new Call(url);
			call2.setOperationName("getDict");
			String re = (String)call2.invoke(new Object[]{usercode,md5, "xzqh"});
			System.out.println(re);
	}
	
	
	//住宿信息报送
	public static void test_postNWBRecrods() throws Exception{
		
		
		MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] buff = m.digest(pwd.getBytes());
		String md5 = new String(Hex.encode(buff));

		org.apache.axis.client.Call call2 = new org.apache.axis.client.Call(url);
		call2.setOperationName("postNWBRecrods");
		
		//准备10个演示用的10个内宾资料，准备批量提供
		List<NbBean> list = new ArrayList<NbBean>();
		//for(int i=0;i<10;i++){
			NbBean nb = new NbBean();
			nb.setPostid(0+"");
			list.add(nb);
		//}
		
		//将10个内宾数组转换为JSON对象字符串表示
		GsonBuilder build = new GsonBuilder();
		Gson gson = build.create();
		String json_list = gson.toJson(list);
		
		//提交并返回结果
		String re = (String)call2.invoke(new Object[]{usercode,md5,"1", json_list});
		System.out.println(re);
	}
	
	//离店操作	
	public static void  test_postLd() throws Exception{
		//将密码进行MD5加密
		MessageDigest m = MessageDigest.getInstance("MD5");
		byte[] buff = m.digest(pwd.getBytes());
		String md5 = new String(Hex.encode(buff));

		//修改调用方式
		org.apache.axis.client.Call call2 = new org.apache.axis.client.Call(url);
		call2.setOperationName("postLd");
		List<TfBean> list = new ArrayList<TfBean>();
		
		//封装参数
		TfBean t1 = new TfBean();
		t1.setPostid("001");
		t1.setLkbm("3401030116201306240018");
			
		TfBean t2 = new TfBean();
		t2.setPostid("001");
		t2.setLkbm("3401030116201306240019");
		
		list.add(t1);
		list.add(t2);
		
		GsonBuilder build = new GsonBuilder();
		Gson gson = build.create();
		String json_list = gson.toJson(list);
		//调用参数
		String re = (String)call2.invoke(new Object[]{usercode,md5,"1",json_list});
		System.out.println(re);
	}
	
	
}	