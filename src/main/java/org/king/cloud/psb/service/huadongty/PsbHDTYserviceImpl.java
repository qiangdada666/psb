package org.king.cloud.psb.service.huadongty;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.king.cloud.psb.dao.WeixinWxpsbDao;
import org.king.cloud.psb.pojo.Wxpms;
import org.king.cloud.psb.service.IWeixinService;
import org.king.cloud.psb.service.impl.SmsServiceImpl;
import org.king.cloud.psb.utils.DateUtils;
import org.king.cloud.psb.utils.JsonListMapUtil;
import org.king.cloud.psb.utils.RedisPool;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hdtytech.jutils.AesUtils;
import com.hdtytech.jutils.ShaUtils;
import com.kbox.commons.network.HttpUtils;

@Service("psbHDTYService")
public class PsbHDTYserviceImpl {
	
	protected Logger logger = Logger.getLogger(this.getClass());
	
	@Resource(name = "smsService")
	SmsServiceImpl smsService;
	@Resource(name="iweixinService")
    IWeixinService iweixinService;
	/*@Value("${hdty.appkey}")
	private String appkey;
	@Value("${hdty.aeskey}")
	private String aeskey;
	@Value("${hdty.requestid}")
	private String requestid;
	@Value("${hdty.companyid}")
	private String companyid;
	@Value("${hdty.url}")
	private String url;*/
	private String url= "http://psb.ihdty.com/PSB/restful/localguest/";
	private String companyid;
	private String aeskey;
	private String appkey;
	private String requestid;
	
	@Resource
	WeixinWxpsbDao weixinWxpmsDao;
	public JSONObject selectWxpmsByPrimaryKey(String id){
    	Wxpms s;
    	s=  iweixinService.selectWxpmsByPrimaryKey(id);
    	JSONObject obj2 = JSON.parseObject(s.getStr2());
    	return obj2;
	 }
	
	/**
	 * 入住
	 * @param obj
	 * @return
	 */
	public String checkIn(JSONObject obj) {
		
		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		appkey = selectWxpmsByPrimaryKey.get("appkey").toString();
		aeskey = selectWxpmsByPrimaryKey.get("aeskey").toString();
		requestid = selectWxpmsByPrimaryKey.get("requestid").toString();
		companyid = selectWxpmsByPrimaryKey.get("companyid").toString();
	
		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		JSONObject idCard = jsonObject.getJSONObject("obj");
		String result = "";
		String realName = idCard.getString("name");
		String certificateCode = idCard.getString("cardNum");
		String hotelId = jsonObject.getString("hotelId");
		HashMap<Object, Object> resultData = new HashMap<>();
		try {
			
			long timeStampSec = System.currentTimeMillis()/1000;
		    String timestamp = String.format("%010d", timeStampSec);
		    HashMap<String, String> map = new HashMap<>();
		    map.put("requestid", requestid);
		    map.put("timestamp", timestamp); //使用十位时间截
		    
		    List<String> s = new ArrayList<String>();
		    s.add(appkey);
		    s.add(requestid);
		    s.add(timestamp);
		    Collections.sort(s);
		    StringBuffer sb = new StringBuffer();
		    for (String string : s) {
				sb.append(string);
			}
		   
		    String signature = ShaUtils.sha256(sb.toString(), "utf-8");
		    map.put("signature", signature);
		    
			JSONObject objs = new JSONObject();
			
			
//			objs.put("name", jsonObject.getString("realName"));
//			objs.put("sex", jsonObject.getString("sex")); 
//			objs.put("nation", jsonObject.getString("nation")); 
//			objs.put("birthday", jsonObject.getString("birthdate").replace("-", ""));
//			objs.put("cardtype", 1);
//			objs.put("cardnum", jsonObject.getString("certificateCode"));
//			objs.put("region", jsonObject.getString("nativeCode"));
//			objs.put("address", jsonObject.getString("address"));
//			objs.put("logintime", jsonObject.getString("checkinTime").replace("-","").replace(":", "").replace(" ", "").substring(0, 12));
//			objs.put("loginroom", jsonObject.getString("roomCode"));
//			objs.put("loginday", jsonObject.getString("loginday"));
//			objs.put("logouttime", jsonObject.getString("checkoutTime").replace("-","").replace(":", "").replace(" ", "").substring(0, 12));
			
			SimpleDateFormat spm=new SimpleDateFormat("yyyyMMddhhmm");
			SimpleDateFormat spm1=new SimpleDateFormat("yyyy/MM/dd hh:mm");
			objs.put("name", idCard.getString("name"));
			objs.put("sex", "男".equals(idCard.getString("sex"))?1:2); 
			objs.put("nation", idCard.getString("nation")); 
			objs.put("birthday", idCard.getString("birthday"));
			objs.put("cardtype", 1);
			objs.put("cardnum", idCard.getString("cardNum"));
			objs.put("region", idCard.getString("cardNum").substring(0, 6));
			objs.put("address", idCard.getString("address"));
			objs.put("logintime", spm.format(spm1.parse(jsonObject.getString("checkinDate"))));
			objs.put("loginroom", jsonObject.getString("roomNum"));
			objs.put("loginday", DateUtils.daysBetween(jsonObject.getString("checkinDate").substring(0, 10),jsonObject.getString("checkoutDate").substring(0, 10)));
			objs.put("logouttime", spm.format(spm1.parse(jsonObject.getString("checkoutDate"))));
			objs.put("gatflag", selectWxpmsByPrimaryKey.get("gatflag").toString());
			
			HashMap<String, String> data1 = new HashMap<>();
			HashMap<String, String> data2 = new HashMap<>();
		    data1.put("photoflag", "0");
		    data1.put("photo", jsonObject.getString("cardimg"));
		    data2.put("photoflag", "1");
		    data2.put("photo", jsonObject.getString("caijiimg"));
		    List<HashMap<String, String>> photos = new ArrayList<HashMap<String,String>>();
			photos.add(data1);
			photos.add(data2);
		    objs.put("photos", photos); 
		    objs.put("companynum", companyid);  
			String data = AesUtils.encryptAnyKey(objs.toJSONString(),aeskey);
			map.put("data", data);
			
		    String testUrl = url+"add";
			Map<String,String> headers = new HashMap<>();
			headers.put("Content-Type", "application/octet-stream");
			headers.put("requestid", requestid);
			headers.put("timestamp", timestamp);
			headers.put("signature", signature);
			byte[] bytes = HttpUtils.webPost(new URL(testUrl), JsonListMapUtil.toJSONString(map).getBytes(), headers, 30000, 30000);
			result = new String(bytes, "utf-8");
			
			JSONObject json = JSONObject.parseObject(result);
			System.out.println(json.get("msg"));
			if("0".equals(json.getString("code"))){
				System.out.println("HDTY-----------success");
				resultData.put("state",1);
				resultData.put("msg", "success");
				String liushui=RedisPool.get(hotelId+jsonObject.getString("orderId")+"psb");
				if(liushui==null){
					RedisPool.set(hotelId+jsonObject.getString("orderId")+"psb", json.get("data").toString()+","+idCard.getString("cardNum"));
				}else {
					RedisPool.set(hotelId+jsonObject.getString("orderId")+"psb", liushui+"-"+json.get("data").toString()+","+idCard.getString("cardNum"));
				}
				resultData.put("data",json.get("data"));
			}else{
				resultData.put("state",0);
				resultData.put("msg", json.get("msg"));
				String content = "【华动泰越】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
				smsService.send("18612239449", content);
				logger.info(new Date() + " 【华动泰越】入住上传psb失败，入住酒店号是:"+hotelId);
			}
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "error");
			String content = "【华动泰越】入住上传psb失败，入住酒店号是:"+hotelId+"，"+"入住人姓名是:"+ realName+"，"+"入住人身份证是:"+certificateCode+"!";
			smsService.send("18612239449", content);
			logger.info(new Date() + " 【华动泰越】入住上传psb失败，入住酒店号是:"+hotelId);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
	}
	
	/*public static void main(String[] args) {
		//RedisPool.set("JDI152UAQ705ONX664FGV4306276244psb", "15010200012018080917592996,150922199501281519");
		System.out.println(RedisPool.get("JDI152UAQ705ONX664FGV4306276244psb"));
	}*/
	/**
	 * 退房
	 * @param obj
	 * @return
	 */
	public String checkout (JSONObject obj) {
		
		String hId=obj.getString("hotelId");
		JSONObject selectWxpmsByPrimaryKey = selectWxpmsByPrimaryKey(hId);
		appkey = selectWxpmsByPrimaryKey.get("appkey").toString();
		aeskey = selectWxpmsByPrimaryKey.get("aeskey").toString();
		requestid = selectWxpmsByPrimaryKey.get("requestid").toString();
		companyid = selectWxpmsByPrimaryKey.get("companyid").toString();
		
		JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
		String result = "";
		//String realName = jsonObject.getString("realName");
		String certificateCode = "";  //jsonObject.getString("certificateCode");
		String liushui ="";
		String hotelId = jsonObject.getString("hotelId");
		HashMap<Object, Object> resultData = new HashMap<>();
		try {
			String serialNum=  RedisPool.get(hotelId+jsonObject.getString("orderId")+"psb");
			String [] str=serialNum.split("-");
			for (int i = 0; i < str.length; i++) {
				String [] strte = str[i].split(",");
				long timeStampSec = System.currentTimeMillis()/1000;
			    String  timestamp = String.format("%010d", timeStampSec);
			    HashMap<String, String> map = new HashMap<>();
			    map.put("requestid", requestid);
			    map.put("timestamp", timestamp); //使用十位时间截
			    
			    List<String> s = new ArrayList<String>();
			    s.add(appkey);
			    s.add(requestid);
			    s.add(timestamp);
			    Collections.sort(s);
			    StringBuffer sb = new StringBuffer();
			    for (String string : s) {
					sb.append(string);
				}
			   
			    String signature = ShaUtils.sha256(sb.toString(), "utf-8");
			    map.put("signature", signature);
			    SimpleDateFormat spm=new SimpleDateFormat("yyyyMMddhhmm");
				SimpleDateFormat spm1=new SimpleDateFormat("yyyy/MM/dd hh:mm");
				JSONObject objs = new JSONObject();
				objs.put("cardnum", strte[1]);
				objs.put("loginroom", jsonObject.getString("roomNum"));
				objs.put("logouttime", spm.format(spm1.parse(jsonObject.getString("checkoutDate"))));
				objs.put("guestNum", strte[0]);
			    objs.put("companynum", companyid);  
			    liushui=strte[0];
			    certificateCode = strte[1];
				String data = AesUtils.encryptAnyKey(objs.toJSONString(),aeskey);
				map.put("data", data);
				
			    String testUrl = url+"quit";
				Map<String,String> headers = new HashMap<>();
				headers.put("Content-Type", "application/octet-stream");
				headers.put("requestid", requestid);
				headers.put("timestamp", timestamp);
				headers.put("signature", signature);
				byte[] bytes = HttpUtils.webPost(new URL(testUrl), JsonListMapUtil.toJSONString(map).getBytes(), headers, 30000, 30000);
				result = new String(bytes, "utf-8");
				
				JSONObject json = JSONObject.parseObject(result);
				System.out.println(json.get("msg"));
				if("0".equals(json.getString("code"))){
					System.out.println("HDTY-----------success");
					resultData.put("state",1);
					resultData.put("msg", "success");
				}else{
					resultData.put("state",0);
					resultData.put("msg", json.get("msg"));
					String content = "【华动泰越】退房上传psb失败，入住酒店号是:"+hotelId+"，"+"流水号是:"+ liushui+"，"+"入住人身份证是:"+certificateCode+"!";
					smsService.send("18612239449", content);
					logger.info(new Date() + " 【华动泰越】退房上传psb失败，入住酒店号是:"+hotelId);
				}
			}
				
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "error");
			String content = "【华动泰越】退房上传psb失败，入住酒店号是:"+hotelId+"，"+"流水号是:"+ liushui+"，"+"入住人身份证是:"+certificateCode+"!";
			smsService.send("18612239449", content);
			logger.info(new Date() + " 【华动泰越】退房上传psb失败，入住酒店号是:"+hotelId);
		} 
		String data = JsonListMapUtil.toJSONString(resultData);
		return data;
		
	}
	
}
