package org.king.cloud.psb.service.xidian.util;

import java.io.UnsupportedEncodingException;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESUtil {
    public static final String KEY_ALGORITHM="AES";
    public static final String CIPHER_ALGORITHM="AES/ECB/PKCS5PADDING";

    public static Key toKey(byte[] key) {
        SecretKey secretKey=new SecretKeySpec(key,KEY_ALGORITHM);
        return secretKey;
    }
    public static String encrypt(String data,String key) throws Exception{
        Key k=toKey(Md5Util.MD5ByUTF8(key).toUpperCase().getBytes());

        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, k);
        //执行操作
        return Base64.encodeBase64String(cipher.doFinal(data.getBytes()));
    }
    public static String decrypt(String data,String key) throws Exception{
        byte[] datas = Base64.decodeBase64(data);
        Key k =toKey(Md5Util.MD5ByUTF8(key).toUpperCase().getBytes());
        Cipher cipher=Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, k);
        return new String(cipher.doFinal(datas));
    }


    public static void main(String[] args) throws UnsupportedEncodingException{
        String str="{\"checkoutTime\":\"2018-05-24\",\"hotelCode\":\"CS-001\",\"serialNum\":\"123456789\"}";
//        System.out.println("原文："+str);
        try {
            //初始化密钥
            String key = Md5Util.MD5ByUTF8("123456").toUpperCase();
            System.out.println("key:" + Md5Util.MD5ByUTF8("cs123456").toUpperCase());
            long beginTime = System.currentTimeMillis();
            //加密数据
            String data = AESUtil.encrypt(str, "cs123456");
            System.out.println("加密后：" + data);
            System.out.println("加密时间："+(System.currentTimeMillis()-beginTime));
            beginTime = System.currentTimeMillis();
//            //解密数据
            System.out.println("解密后：" + AESUtil.decrypt(data, "cs123456"));
            System.out.println("解密时间："+(System.currentTimeMillis()-beginTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
