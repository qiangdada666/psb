package org.king.cloud.psb.utils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
/**
 * 
 * <pre>项目名称：king-cloud-psb    
 * 类名称：RedisPool    
 * 类描述：    
 * 创建人：MaQiang    
 * 创建时间：2018年8月1日 下午3:05:08    
 * 修改人：MaQiang    
 * 修改时间：2018年8月1日 下午3:05:08    
 * 修改备注：       
 * @version </pre>
 */
	public final class RedisPool {
		 //Redis服务器IP
	    private static String ADDR = "39.104.68.154";

	    //Redis的端口号
	    private static int PORT = 6379;

	    //访问密码
	    private static String AUTH = "umg12580";

	    //可用连接实例的最大数目，默认值为8；
	    //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
	    private static int MAX_ACTIVE = 1024;

	    //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
	    private static int MAX_IDLE = 200;

	    //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
	    private static int MAX_WAIT = 10000;

	    private static int TIMEOUT = 10000;

	    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
	    private static boolean TEST_ON_BORROW = true;

	    private static JedisPool jedisPool = null;

	    /**
	     * 初始化Redis连接池
	     */
	    static {
	        try {
	            JedisPoolConfig config = new JedisPoolConfig();
	            config.setMaxTotal(MAX_ACTIVE);
	            config.setMaxIdle(MAX_IDLE);
	            config.setMaxWaitMillis(MAX_WAIT);
	            config.setTestOnBorrow(TEST_ON_BORROW);
	            jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    public synchronized static Jedis getJedis() {
	        try {
	            if (jedisPool != null) {
	                Jedis resource = jedisPool.getResource();
	                return resource;
	            } else {
	                return null;
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    /**
	     * 释放jedis资源
	     * @param jedis
	     */
	    public static void returnResource(final Jedis jedis) {
	        if (jedis != null) {
	            jedisPool.returnResource(jedis);
	        }
	    }
	    /**
	     * <pre>exists(判断缓存中key是否存在)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:03:35    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:03:35    
	     * 修改备注： 
	     * @param key
	     * @return</pre>
	     */
	    public static Boolean exists(String key) {
	        Jedis jedis = null;
	        try {
	            jedis = jedisPool.getResource();
	            return jedis.exists(key.getBytes());
	        }catch(Exception e) {
	            e.printStackTrace();
	            return null;
	        }finally{
	            if(jedis != null)
	            {
	                jedis.close();
	            }
	        }
	    }
	    /**
	     * <pre>set(保存字符串到缓存中)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:04:24    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:04:24    
	     * 修改备注： 
	     * @param key
	     * @param value</pre>
	     */
	    public static void set(String key,String value){
	    	Jedis jedis = null;
	        try {
	            jedis = jedisPool.getResource();
	            getJedis().set(key, value);
	        }catch(Exception e) {
	            e.printStackTrace();
	        }finally{
	            if(jedis != null)
	            {
	                jedis.close();
	            }
	        }
	    	
	    }
	    /**
	     * <pre>get(通过key 获取缓存中value)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:05:45    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:05:45    
	     * 修改备注： 
	     * @param key
	     * @return</pre>
	     */
	    public static String get(String key){
	    	Jedis jedis = null;
	        try {
	            jedis = jedisPool.getResource();
	            return getJedis().get(key);
	        }catch(Exception e) {
	            e.printStackTrace();
	            return null;
	        }finally{
	            if(jedis != null)
	            {
	                jedis.close();
	            }
	        }
	    }
	  //
	    /**
	     * <pre>setObj(对象序存到缓存中)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:06:19    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:06:19    
	     * 修改备注： 
	     * @param key
	     * @param obj</pre>
	     */
	    public static void setObj(String key,Object obj){
	    	
	        String serStr = null;
	        try {
	            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
	            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);  
	            objectOutputStream.writeObject(obj);    
	            serStr = byteArrayOutputStream.toString("ISO-8859-1");  
	            serStr = java.net.URLEncoder.encode(serStr, "UTF-8");  
	              
	            objectOutputStream.close();  
	            byteArrayOutputStream.close();
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        set(key,serStr);
	    }
	    
	    /**
	     * <pre>getObj(通过key 取出缓存中对象)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:07:17    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:07:17    
	     * 修改备注： 
	     * @param key
	     * @return</pre>
	     */
	    public static Object getObj(String key){
	    	String serStr=get(key);
	    	Object newObj = null;
	    	if(serStr!=null){
		        try {
		            String redStr = java.net.URLDecoder.decode(serStr, "UTF-8");  
		            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(redStr.getBytes("ISO-8859-1"));  
		            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);   
		            newObj = objectInputStream.readObject();
		            objectInputStream.close();  
		            byteArrayInputStream.close();
		        } catch (UnsupportedEncodingException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        } catch (ClassNotFoundException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        } catch (IOException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
	    	}    
	        return newObj;
	    }
	    /**
	     * <pre>delkeyObj(删除key)   
	     * 创建人：MaQiang   
	     * 创建时间：2018年8月1日 下午4:08:57    
	     * 修改人：MaQiang  
	     * 修改时间：2018年8月1日 下午4:08:57    
	     * 修改备注： 
	     * @param key
	     * @return</pre>
	     */
	    public Long delkeyObj(String key) {
	        Jedis jedis = null;
	        try {
	            jedis = jedisPool.getResource();
	            return jedis.del(key.getBytes());
	        }catch(Exception e) {
	            e.printStackTrace();
	            return null;
	        }finally{
	            if(jedis != null)
	            {
	                jedis.close();
	            }
	        }
	    }
	}

