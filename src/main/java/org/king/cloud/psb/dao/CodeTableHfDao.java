package org.king.cloud.psb.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CodeTableHfDao {
	
	@Select("select code from code_table_hf where name = #{name} and type = 'mz' ")
	@Results(
			@Result(property  = "code", column = "code")
			
			)

    String selectByParamOfNation(@Param("name")String name);

	//@Insert("insert into code_table_hf (id,code,type,typename,name) values(999999,999,999,'嘿嘿','哈哈哈')")
	//String selectByParamOfNation2(String names);

}