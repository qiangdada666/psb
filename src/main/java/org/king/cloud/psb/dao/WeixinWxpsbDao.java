package org.king.cloud.psb.dao;



import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.king.cloud.psb.pojo.Wxpms;

@Mapper
public interface WeixinWxpsbDao {
	@Select("select psbkey from weixin_wxpms where mpcfgid = #{mpcfgid}")
	@Results({
        @Result(property = "psbkey", column = "psbkey")
	})
    String selectByPrimaryKey(@Param("mpcfgid")String mpcfgid);

	
	@Select("select pmskey,appkey,username,passwords,str1,str2 from weixin_wxpms where mpcfgid = #{mpcfgid}")
	@Results({
        @Result(property = "pmskey", column = "pmskey"),
        @Result(property = "appkey", column = "appkey"),
        @Result(property = "username", column = "username"),
        @Result(property = "passwords", column = "passwords"),
        @Result(property = "str1", column = "str1"),
        @Result(property = "str2", column = "str2")
	})
	Wxpms selectWxpmsByPrimaryKey(@Param("mpcfgid")String mpcfgid);
}