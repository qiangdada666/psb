package org.king.cloud.psb.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.king.cloud.psb.service.IWeixinService;
import org.king.cloud.psb.service.dajiang.PsbDJServiceImpl;
import org.king.cloud.psb.service.guotai.PsbGTserviceImpl;
import org.king.cloud.psb.service.hefei.PsbHFServiceImpl;
import org.king.cloud.psb.service.huadongty.PsbHDTYserviceImpl;
import org.king.cloud.psb.service.xidian.PsbXDserviceImpl;
import org.king.cloud.psb.service.yichang.PsbYCServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@RestController
@RequestMapping("psbController")
public class PsbController {
		@Resource(name="iweixinService")
	    IWeixinService iweixinService;
		
		@Resource(name = "psbXDservice")
		PsbXDserviceImpl psbXDservice;
		
		@Resource(name = "psbGTservice")
		PsbGTserviceImpl psbGTservice;
		
		@Resource(name = "psbDJService")
		PsbDJServiceImpl psbDJService;
		
		@Resource(name = "psbYCService")
		PsbYCServiceImpl psbYCService;
		
		@Resource(name = "psbHDTYService")
		PsbHDTYserviceImpl psbHDTYService;
		
		@Resource(name = "psbHFService")
		PsbHFServiceImpl psbHFService;
		
		/***
	     * 境内旅客登记、修改 境内旅客住宿登记信息、维护（信息修改、换房）信息上报
	     * param xmlStr
	     * @return Json
	     * @throws Exception
	     */
	    @RequestMapping(value = "/checkin", method = {RequestMethod.POST, RequestMethod.GET})
	    @ResponseBody
	    public String checkin(String xmlStr) throws Exception {
	    	JSONObject obj = JSON.parseObject(xmlStr);
	    	
//	    	JSONObject obj = new JSONObject();
	    	
//			obj.put("hotelId", "DEA152TML447CZN332XJH5701862");
//			obj.put("serialNum", "");// 流水号 
//			obj.put("realName", "哇哇"); // 旅客姓名
//			obj.put("sex", "1"); // 性别
//			obj.put("nation", "01"); // 民族
//			obj.put("birthdate", "1990-01-01");// 旅客生日    可为空
//			obj.put("certificateCode", "360481198007020214");// 证件号码
//			obj.put("phone", "13411112222");// 手机号  可为空
//			obj.put("nativeCode", "410381");// 户籍地
//			obj.put("address", "户籍地址");// 户籍地址
//			obj.put("hotelCode", "CS-001");// 旅馆 Code
//			obj.put("checkinTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));// 入住时间
//			obj.put("checkoutTime", "");// 离店时间  可为空
//			obj.put("roomCode", "603");// 房间号
//			obj.put("photo", "xxxx");// 
//			obj.put("scenePhoto", "xxx");// 
//			obj.put("expired", "");// 身份证有效期
//			obj.put("hotelno", "4205000169");// 身份证有效期
	    	
	    	String code=getPSBVersions(obj.getString("hotelId").toString());
	    	String date="";
	    	switch (code){
	    		case "xd":
	    			date= psbXDservice.checkIn(obj);
	    			break;
	    		case "gt":
	    			date= psbGTservice.checkIn(obj);
	    			break;
	    		case "dj":
	    			date= psbDJService.checkIn(obj);
	    			break;
	    		case "yc":
	    			date= psbYCService.checkIn(obj);
	    			break;
	    		case "hdty":
	    			date= psbHDTYService.checkIn(obj);
	    			break;
	    		case "hf":
	    			date= psbHFService.checkIn(obj);
	    			break;
	    	}
	    	return date;
	    }
	    
	    /***
	     * 境内旅客登记、修改 境内旅客住宿登记信息、维护（信息修改、换房）信息上报
	     * param xmlStr
	     * @return Json
	     * @throws Exception
	     */
	    @RequestMapping(value = "/checkout", method = {RequestMethod.POST, RequestMethod.GET})
	    @ResponseBody
	    public String checkout(String xmlStr) throws Exception {
	    	JSONObject obj = JSON.parseObject(xmlStr);
	    	
//	    	JSONObject obj = new JSONObject();
//	    	
//	    	obj.put("hotelId", "DEA152TML447CZN332XJH5701862");
//			obj.put("serialNum", "SN201805260000774");// 流水号 	
//	    	obj.put("hotelCode", "CS-001");// 旅馆 Code
//	    	obj.put("checkoutTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));// 离店时间  可为空
//	    	obj.put("roomCode", "603");// 房间号
//	    	obj.put("idCard", "370102199001018801");// 身份证号	
//	    	obj.put("checkInTime", "2018-05-25 14:32:00");//
//	    	obj.put("hotelno", "4205000169");// 
	    	
	    	String code=getPSBVersions(obj.getString("hotelId").toString());
	    	String date="";
	    	switch (code){
	    	case "xd":
	    		date= psbXDservice.checkout(obj);
	    		break;
	    	case "gt":
	    		date= psbGTservice.checkout(obj);
	    		break;
	    	case "dj":
	    		date= psbDJService.checkout(obj);
	    		break;
	    	case "yc":
	    		date= psbYCService.checkout(obj);
	    		break;
	    	case "hdty":
    			date= psbHDTYService.checkout(obj);
    			break;
	    	case "hf":
	    		date= psbHFService.checkout(obj);
	    		break;
	    	}
	    	return date;
	    }

	    public String getPSBVersions(String Id){
	    	return iweixinService.selectByPrimaryKey(Id);
	    }
}
